<?php
/**
 * Hooks for nav menus
 */

function konte_page_menu_args( $args ) {
	$args['container']  = 'ul';
	$args['menu_class'] = 'menu nav-menu';
	$args['before']     = '';
	$args['after']      = '';

	return $args;
}

add_filter( 'wp_page_menu_args', 'konte_page_menu_args' );

/**
 * Add a walder object for all nav menus
 *
 * @since  1.0.0
 *
 * @param  array $args The default args
 *
 * @return array
 */
function konte_nav_menu_args( $args ) {
	if ( in_array( $args['theme_location'], array( 'primary', 'secondary' ) ) ) {
		// Not using mega menu for vertical menu.
		if ( 'v10' == konte_get_header_layout() ) {
			$args['menu_class'] .= ' nav-menu--submenu-' . konte_get_option( 'header_vertical_submenu_toggle' );

			return $args;
		}

		// Remove fallback to pages.
		$args['fallback_cb'] = false;

		// Only support mega menu if the Konte Addons plugin installed.
		if ( class_exists( 'Konte_Addons_Mega_Menu_Walker' ) ) {
			if ( 'mobile-menu__nav' != $args['container_class'] ) {
				$args['walker'] = new Konte_Addons_Mega_Menu_Walker;
			}
		}

		// Add custom class for carets.
		if ( konte_get_option( 'header_menu_caret_submenu' ) ) {
			$args['menu_class'] .= ' nav-menu--submenu-has-caret';
		}
	}

	return $args;
}

add_filter( 'wp_nav_menu_args', 'konte_nav_menu_args' );

function konte_menu_item_caret( $title, $menu_item, $args ) {
	// Only support main navigations.
	if ( ! in_array( $args->theme_location, array( 'primary', 'secondary' ) ) ) {
		return $title;
	}

	if ( 'mobile-menu__nav' == $args->container_class ) {
		return $title;
	}

	if ( ! konte_get_option( 'header_menu_caret' ) ) {
		return $title;
	}

	$caret = konte_get_option( 'header_menu_caret_arrow' );

	switch ( $caret ) {
		case 'plus_text':
			$caret = '+';
			break;

		case 'plus':
			$caret = '<i class="fa fa-plus"></i>';
			break;

		default:
			$caret = '<i class="fa fa-' . esc_attr( $caret ) . '-down"></i>';
			break;
	}

	if ( in_array( 'menu-item-has-children', $menu_item->classes ) ) {
		$title .= '<span class="caret">' . $caret . '</span>';
	}

	return $title;
}

add_filter( 'nav_menu_item_title', 'konte_menu_item_caret', 10, 3 );