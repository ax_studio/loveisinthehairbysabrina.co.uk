<?php
/**
 * Template part for displaying the wishlist icon
 *
 * @package Konte
 */
if ( ! function_exists( 'WC' ) ) {
	return;
}
if ( function_exists( 'Soo_Wishlist' ) ) {
	printf(
		'<div class="header-wishlist">
			<a href="%s" class="wishlist-contents">
				%s
				<span class="counter wishlist-counter">%s</span>
			</a>
		</div>',
		esc_url( soow_get_wishlist_url() ),
		konte_svg_icon( 'icon=heart-o&echo=0' ),
		soow_count_products()
	);
} elseif ( defined( 'YITH_WCWL' ) ) {
	printf(
		'<div class="header-wishlist">
			<a href="%s" class="wishlist-contents">
				%s
				<span class="counter wishlist-counter">%s</span>
			</a>
		</div>',
		esc_url( get_permalink( yith_wcwl_object_id( get_option( 'yith_wcwl_wishlist_page_id' ) ) ) ),
		konte_svg_icon( 'icon=heart-o&echo=0' ),
		yith_wcwl_count_products()
	);
}
