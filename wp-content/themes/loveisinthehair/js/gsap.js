gsap.registerPlugin(ScrollTrigger);

// Heading animation
gsap.utils.toArray("h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6").forEach(heading => {
    var tl = gsap.timeline({
        scrollTrigger: {
            trigger: heading,
            toggleActions: "restart pause resume reset",
            start: "top 100%"
        }
    });

    tl.fromTo(heading, {
            scale: 0.9,
            opacity: 0,
        },
        {
            duration: 0.7,
            scale: 1,
            opacity: 1,
            delay: 0.3
        });
});
